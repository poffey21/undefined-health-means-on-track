# Undefined Health Means On Track

The intent of this use case is to be able to see all issues that 
are either `At Risk` or `Need Attention`. In order to do that,
all issues need to be marked as `On Track` given that GitLab 
does not allow you to leverage `OR` statements in Health Status 
Lookups (see [issue comment](https://gitlab.com/groups/gitlab-org/-/epics/9260#note_1315703463)).

*What this does*: In a given group, goes through all of its issues
and sets the Health Status to On Track if it is not set. 

Once set, you can use `Health Status != On Track` logic to find all
issues that are either `At Risk` or `Needs Attention`.

## Getting started

1. Fork this project
1. Set your Access Token with the appropriate API Access as `PRIVATE_TOKEN` in your `Settings` > `CI/CD` > `Variables`.
1. Navigate to `Pipelines` > `Run Pipeline` > `New Pipeline`
1. Populate the `GROUP_ID` variable with your Group's ID (found on the Group Overview page)
1. Click `Run Pipeline`!

