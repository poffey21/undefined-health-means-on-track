import os
from gitlab import Gitlab
from gitlab.exceptions import GitlabCreateError
from envparse import Env
from pprint import pprint

# You can do the same with `os.environ.get` but this is 
# really helpful as you can define a `.env` file for local
# development that contains all of the Predefined GitLab CI
# Variables.
env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')

# GitLab API variables
private_token_override = env('PRIVATE_TOKEN_OVERRIDE', default='')

if private_token_override:
    private_token = private_token_override
else:
    private_token = env('PRIVATE_TOKEN')

group_id = env('GROUP_ID')
origin = env('GITLAB_ORIGIN')



def main():
    """
    Leverage Python GitLab library to run through all open issues that have no 
    status set and set their status.
    """
    gl = Gitlab(origin, private_token=private_token)
    gl.auth()

    projects = gl.groups.get(group_id).projects.list(get_all=True, include_subgroups=True)

    for group_project in projects:
        project = gl.projects.get(group_project.id)
        for issue in project.issues.list(get_all=True, state='opened'):
            if issue.attributes['issue_type'] == 'issue' and issue.attributes['health_status'] is None:
                try:
                    issue.discussions.create({
                        'body': '/health_status on_track'
                    })
                except GitlabCreateError as e:
                    if 'Set health status to on_track.' not in str(e):
                        print(issue.title)
                        print(f'{issue.id}: {e}')
                        exit(1)
                print(f'Added a message to the discussion on: {issue.title}')
                

if __name__ == '__main__':
    main()
